package com.orbit.jira.rest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.orbit.jira.dao.JiraSRDao;
import com.orbit.jira.issues.service.JsonParser;
import com.orbit.jira.pojo.JiraIssue;
import com.orbit.jira.utility.ConfigurationUtility;
import com.orbit.jira.utility.DBUtility;
import com.orbit.jira.utility.DateUtility;
import com.orbit.jira.utility.JiraUtility;

/**
 * 
 * @author GiriKothwal
 *
 */
public class JiraClient {

	private final static Logger logger = LoggerFactory.getLogger(JiraClient.class);

	/**
	 * Used for retrieving weekly open, closed and new SRS from JIRA and Storing
	 * in to Database.. To make weekly analysis on JIRA Work Flow.
	 * 
	 * @param processingDate
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws ClassNotFoundException
	 */
	public void populateSRInformation()
			throws URISyntaxException, IOException, SQLException, ParseException, ClassNotFoundException {
		// Make sure all the configuration needs are loaded well
		if (!ConfigurationUtility.loadCheck()) {
			logger.info("Existing, Unable to load required configuration setup");
			System.out.println("Existing, Unable to load required configuration setup");
			return;
		}

		// New SRS
		String newSRSJqlQuery = ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewNewSRSQuery);
		// Closed SRS
		String closedSRSJqlQuery = ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewClosedSRSQuery);
		// Open SRS
		String openSRSJqlQuery = ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewOpenSRSQuery);
		// History SRS
		String historySRSJqlQuery = ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewHistorySRSQuery);

		String authStringEnc = JiraUtility.getEncodedAuthenticationString(
				ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraUsername),
				ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraAPIKey));

		populateJiraIsuues(JiraUtility.FLAG_OPEN_SRS, openSRSJqlQuery, authStringEnc);
		populateJiraIsuues(JiraUtility.FLAG_NEW_SRS, newSRSJqlQuery, authStringEnc);
		populateJiraIsuues(JiraUtility.FLAG_CLOSED_SRS, closedSRSJqlQuery, authStringEnc);
		populateJiraIsuues(JiraUtility.FLAG_HISTORY_SRS, historySRSJqlQuery, authStringEnc);
	}

	private static void populateJiraIsuues(String typeOfSRs, String srsJqlQuery, String authStringEnc)
			throws ClassNotFoundException, SQLException, URISyntaxException, FileNotFoundException, IOException,
			ParseException {
		int jiraWeekId = 0;
		String urlString = null;
		Connection con = null;

		logger.info("inside populateJiraIsuues  " + typeOfSRs);

		try {
			con = DBUtility.getDBConection();
			logger.info("Getting Connection is Successful..  " + con);

			jiraWeekId = JiraSRDao.getJiraWeekId(typeOfSRs, con);
			logger.info("Getting Jira WeekId ..  " + jiraWeekId);

			if (typeOfSRs.contains(JiraUtility.FLAG_HISTORY_SRS))
				JiraSRDao.checkHistory(con, jiraWeekId);

			urlString = JiraUtility.constructEncodedUrl(
					ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraBaseURL), srsJqlQuery);
			logger.info("Constructed encode URL ..  " + urlString);

			if (jiraWeekId != 0) {
				List<String> jsonList = JiraUtility.callJiraRESTService(urlString, authStringEnc);
				logger.info("Getting Json List .............  ");

				if (jsonList != null) {
					JsonParser jsonParser = new JsonParser();
					for (String jsonString : jsonList) {
						List<JiraIssue> listOfPojo = jsonParser.parseJiraIssues(jsonString);
						JiraSRDao.inserting_JIRA_issues_in_DB(listOfPojo, jiraWeekId, typeOfSRs,
								DateUtility.getUtilDate(JiraSRDao.getDBSysDate(con)), con);
					}
					System.out.println("Data Added Sucessfully .....  "
							+ DateUtility.getUtilDate(JiraSRDao.getDBSysDate(con)) + "  for  " + typeOfSRs);
					logger.info("Data Added Sucessfully .....  " + DateUtility.getUtilDate(JiraSRDao.getDBSysDate(con))
							+ "  for  " + typeOfSRs);
				}
			} else {
				System.out.println("Data already existed with this date ==  "
						+ DateUtility.getUtilDate(JiraSRDao.getDBSysDate(con)) + "  for  " + typeOfSRs);
				logger.info("Data already existed with this date ==  "
						+ DateUtility.getUtilDate(JiraSRDao.getDBSysDate(con)) + "  for  " + typeOfSRs);
			}
		} finally {
			DBUtility.closeConnection(con);
		}
	}

	@Override
	public String toString() {
		return "JiraClient [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
}
