package com.orbit.jira.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import com.orbit.jira.pojo.JiraIssue;
import com.orbit.jira.utility.DBUtility;
import com.orbit.jira.utility.DateUtility;
import com.orbit.jira.utility.JiraUtility;

/**
 * 
 * @author GiriKothwal
 *
 */
public class JiraSRDao {

	static String OSJiraNewSRs = "insert into OS_Jira_New_SRs values(OS_Jira_New_SRs_s.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	static String OSJiraClosedSRs = "insert into OS_Jira_Closed_SRs values(OS_JIRA_CLOSED_SRS_s.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	static String OSJiraOpenSRs = "insert into OS_Jira_Open_SRs values(OS_Jira_Open_SRs_s.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	static String OSJiraHistorySRs = "insert into OS_Jira_SR_history values(OS_JIRA_SR_HISTORY_s.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	static String closedJiraWeekId = "{? = call get_closed_jira_week_id(?)}";
	static String newJiraWeekId = "{? = call get_new_jira_week_id(?)}";
	static String openJiraWeekId = "{? = call get_open_jira_week_id(?)}";
	static String historyJiraWeekId = "{? = call get_history_jira_week_id(?)}";
	static String historybkpJiraWeekId = "{call get_history_bkp_jira_week_id(?)}";
	static String weekDimensionsId = "{? = call get_week_dimensions_id(?)}";
	static String get_sysdate_in_IST = "{? = call get_sysdate_in_IST()}";

	/**
	 * We are getting weekId based on current date. Checking weather any data is
	 * existed with that weekId in table. If exist we are returning 0 and if not
	 * exist then we are returning WeekId to the caller.
	 * 
	 * @param currentDate
	 * @param typeOfSRs
	 * @param con
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int getJiraWeekId(String typeOfSRs, Connection con) throws ClassNotFoundException, SQLException {
		CallableStatement weekDimentionIDcstmt = null;
		CallableStatement callingExistedcstmtIds = null;

		int value = 0;

		try {

			// Reading current date from DB
			java.sql.Date currentDBdate = getDBSysDate(con);

			weekDimentionIDcstmt = con.prepareCall(weekDimensionsId);
			weekDimentionIDcstmt.registerOutParameter(1, Types.INTEGER);
			weekDimentionIDcstmt.setDate(2, currentDBdate);// new
															// java.sql.Date(currentDate.getTime()));
			int cal_count = weekDimentionIDcstmt.executeUpdate();

			if (cal_count > 0) {
				if (typeOfSRs.contains(JiraUtility.FLAG_NEW_SRS)) {
					value = checkWeekIdExisted(weekDimentionIDcstmt, callingExistedcstmtIds, con, newJiraWeekId);
				} else if (typeOfSRs.contains(JiraUtility.FLAG_CLOSED_SRS)) {
					value = checkWeekIdExisted(weekDimentionIDcstmt, callingExistedcstmtIds, con, closedJiraWeekId);
				} else if (typeOfSRs.contains(JiraUtility.FLAG_OPEN_SRS)) {
					value = checkWeekIdExisted(weekDimentionIDcstmt, callingExistedcstmtIds, con, openJiraWeekId);
				} else {
					value = checkWeekIdExisted(weekDimentionIDcstmt, callingExistedcstmtIds, con, historyJiraWeekId);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			DBUtility.closeStatement(callingExistedcstmtIds);
			DBUtility.closeStatement(weekDimentionIDcstmt);
		}
		return value;
	}

	public static int checkWeekIdExisted(CallableStatement weekDimentionIDcstmt,
			CallableStatement callingExistedcstmtIds, Connection con, String jiraWeekId) throws SQLException {
		int value = 0;
		int weekId = weekDimentionIDcstmt.getInt(1) - 1;
		callingExistedcstmtIds = con.prepareCall(jiraWeekId);
		callingExistedcstmtIds.registerOutParameter(1, Types.INTEGER);
		callingExistedcstmtIds.setInt(2, weekId);
		callingExistedcstmtIds.execute();

		if (callingExistedcstmtIds.getInt(1) == 0) {
			value = weekId;
		}
		return value;
	}

	/**
	 * Checking and inserting values into history table on base of week id - 2.
	 * 
	 * @param con
	 * @param weekId
	 * @throws SQLException
	 */
	public static void checkHistory(Connection con, int weekId) throws SQLException {
		CallableStatement cstmt = null;
		weekId = weekId - 1;
		cstmt = con.prepareCall(historybkpJiraWeekId);
		cstmt.setInt(1, weekId);
		cstmt.execute();
	}

	/**
	 * Inserting JIRA issues in Data base.
	 * 
	 * @param listOfPojo
	 * @param weekId
	 * @param typeOfSRs
	 * @param currentDate
	 * @param con
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static void inserting_JIRA_issues_in_DB(List<JiraIssue> listOfPojo, int weekId, String typeOfSRs,
			Date currentDate, Connection con) throws SQLException, ClassNotFoundException {
		PreparedStatement stmt = null;

		try {
			if (typeOfSRs.contains(JiraUtility.FLAG_NEW_SRS)) {
				stmt = con.prepareStatement(OSJiraNewSRs);
			} else if (typeOfSRs.contains(JiraUtility.FLAG_CLOSED_SRS)) {
				stmt = con.prepareStatement(OSJiraClosedSRs);
			} else if (typeOfSRs.contains(JiraUtility.FLAG_HISTORY_SRS)) {
				stmt = con.prepareStatement(OSJiraHistorySRs);
			} else {
				stmt = con.prepareStatement(OSJiraOpenSRs);
			}

			for (JiraIssue jirapojo : listOfPojo) {

				stmt.setString(1, jirapojo.getSrKey());
				stmt.setString(2, jirapojo.getAssignee());
				stmt.setString(3, jirapojo.getReporter());
				stmt.setString(4, jirapojo.getPriority());
				stmt.setString(5, jirapojo.getStatus());

				stmt.setDate(6, DateUtility.getSqlDate(jirapojo.getCreatedDate()));
				stmt.setDate(7, DateUtility.getSqlDate(jirapojo.getUpdatedDate()));

				if (jirapojo.getResolvedDate() != null)
					stmt.setDate(8, DateUtility.getSqlDate(jirapojo.getResolvedDate()));
				else
					stmt.setDate(8, null);

				stmt.setString(9, jirapojo.getComponents());
				stmt.setString(10, jirapojo.getCustomer());
				stmt.setString(11, jirapojo.getSummary());

				if (typeOfSRs.contains(JiraUtility.FLAG_CLOSED_SRS) || typeOfSRs.contains(JiraUtility.FLAG_NEW_SRS)
						|| typeOfSRs.contains(JiraUtility.FLAG_HISTORY_SRS)) {
					stmt.setInt(12, weekId);
					stmt.setString(13, jirapojo.getRegression());

					if (jirapojo.getLabels() != null)
						stmt.setString(14, jirapojo.getLabels());
					else
						stmt.setString(14, null);
				}

				if (typeOfSRs.contains(JiraUtility.FLAG_OPEN_SRS)) {
					stmt.setInt(12, DateUtility.getSubtractDate(currentDate, jirapojo.getCreatedDate()));
					stmt.setString(13,
							getAgeBucket(DateUtility.getSubtractDate(currentDate, jirapojo.getCreatedDate())));
					stmt.setInt(14, DateUtility.getSubtractDate(currentDate, jirapojo.getUpdatedDate()));
					stmt.setString(15, getlastUpdatedAgingBucket(
							DateUtility.getSubtractDate(currentDate, jirapojo.getUpdatedDate())));
					stmt.setInt(16, weekId);
					stmt.setString(17, jirapojo.getRegression());

					if (jirapojo.getLabels() != null)
						stmt.setString(18, jirapojo.getLabels());
					else
						stmt.setString(18, null);
				}
				stmt.execute();
			}
		} finally {
			DBUtility.closeStatement(stmt);
		}
	}

	// Age Calculation
	public static String getAgeBucket(int age) {
		String agingBucket = null;

		if (age > -1 && age <= 5)
			agingBucket = "0 to 05 Days";
		else if (age > 5 && age <= 10)
			agingBucket = "06 to 10 Days";
		else if (age > 10 && age <= 20)
			agingBucket = "11 to 20 Days";
		else if (age > 20 && age <= 30)
			agingBucket = "21 to 30 Days";
		else if (age > 30)
			agingBucket = "Above 30 Days";
		else
			agingBucket = "Unknown";

		return agingBucket;
	}

	public static String getlastUpdatedAgingBucket(int age) {
		String agingBucket = null;

		if (age > -1 && age <= 30)
			agingBucket = "01 to 30 Days";
		else if (age > 30 && age <= 60)
			agingBucket = "31 to 60 Days";
		else if (age > 60 && age <= 90)
			agingBucket = "61 to 90 Days";
		else if (age > 90 && age <= 120)
			agingBucket = "91 to 120 Days";
		else if (age > 120)
			agingBucket = "Above 120 Days";
		else
			agingBucket = "Unknown";

		return agingBucket;
	}

	public static java.sql.Date getDBSysDate(Connection con) {
		CallableStatement currentDatecstmt = null;
		java.sql.Date currentDBdate = null;

		try {
			currentDatecstmt = con.prepareCall(get_sysdate_in_IST);
			currentDatecstmt.registerOutParameter(1, Types.DATE);
			currentDatecstmt.executeUpdate();
			currentDBdate = currentDatecstmt.getDate(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentDBdate;
	}
}
