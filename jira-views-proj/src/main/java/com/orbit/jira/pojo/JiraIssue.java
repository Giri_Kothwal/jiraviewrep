package com.orbit.jira.pojo;

import java.util.Date;

/**
 * 
 * @author GiriKothwal
 *
 */
public class JiraIssue {

	public static final String FLAG_NAME = "name";
	public static final String FLAG_DISPLAY_NAME = "displayName";
	public static final String FLAG_REGRESSION = "Regression";

	private int srId;
	private String srKey;
	private String assignee;
	private String reporter;
	private String priority;
	private String status;
	private Date createdDate;
	private Date updatedDate;
	private Date resolvedDate;
	private String components;
	private String customer;
	private String summary;
	private int week_id;
	private String regression;
	private String labels;

	public int getSrId() {
		return srId;
	}

	public void setSrId(int srId) {
		this.srId = srId;
	}

	public String getSrKey() {
		return srKey;
	}

	public void setSrKey(String srKey) {
		this.srKey = srKey;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getComponents() {
		return components;
	}

	public void setComponents(String components) {
		this.components = components;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getWeek_id() {
		return week_id;
	}

	public void setWeek_id(int week_id) {
		this.week_id = week_id;
	}

	public String getRegression() {
		return regression;
	}

	public void setRegression(String regression) {
		this.regression = regression;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public static String getFlagName() {
		return FLAG_NAME;
	}

	public static String getFlagDisplayName() {
		return FLAG_DISPLAY_NAME;
	}

	public static String getFlagRegression() {
		return FLAG_REGRESSION;
	}

	@Override
	public String toString() {
		return "JiraIssue [srId=" + srId + ", srKey=" + srKey + ", assignee=" + assignee + ", reporter=" + reporter
				+ ", priority=" + priority + ", status=" + status + ", createdDate=" + createdDate + ", updatedDate="
				+ updatedDate + ", resolvedDate=" + resolvedDate + ", components=" + components + ", customer="
				+ customer + ", summary=" + summary + ", week_id=" + week_id + ", regression=" + regression
				+ ", labels=" + labels + "]";
	}
}