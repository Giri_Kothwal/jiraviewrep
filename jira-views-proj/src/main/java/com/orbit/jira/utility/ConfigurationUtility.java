package com.orbit.jira.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 
 * @author GiriKothwal
 *
 */
public class ConfigurationUtility {

	public static final String jiraBaseURL = "jira.base-url";
	public static final String jiraUsername = "jira.username";
	public static final String jiraAPIKey = "jira.api-key";

	public static final String jiraViewDataSourceDriverClassName = "jira-views.data-source.drive-class";
	public static final String jiraViewDataSourceJDBCUrl = "jira-views.data-source.jdbc-url";
	public static final String jiraViewDataSourceUsername = "jira-views.data-source.username";
	public static final String jiraViewDataSourcePassword = "jira-views.data-source.password";

	public static final String jiraViewNewSRSQuery = "query.srs.new";
	public static final String jiraViewClosedSRSQuery = "query.srs.closed";
	public static final String jiraViewOpenSRSQuery = "query.srs.open";
	public static final String jiraViewHistorySRSQuery = "query.srs.history";

	private static final String propertiesFileName = "/connection.properties";
	private static final String queriesFileName = "/queries.properties";

	private static final Properties appProperties = new Properties();

	static {
		InputStream propsStream = null;
		InputStream queriesStream = null;
		try {
			propsStream = ConfigurationUtility.class.getResourceAsStream(propertiesFileName);
			appProperties.load(propsStream);
			queriesStream = ConfigurationUtility.class.getResourceAsStream(queriesFileName);
			appProperties.load(queriesStream);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			if (propsStream != null) {
				try {
					propsStream.close();
				} catch (IOException ioException2) {
					System.out.println("ERROR [EATEN] while closing properties stream : " + ioException2);
				}
			}
			if (queriesStream != null) {
				try {
					queriesStream.close();
				} catch (IOException ioException2) {
					System.out.println("ERROR [EATEN] while closing queries stream : " + ioException2);
				}
			}
		}
	}

	public static String getPropertyValue(String propertyKey) {
		return appProperties.getProperty(propertyKey);
	}

	public static boolean loadCheck() {
		return ("done".equalsIgnoreCase(appProperties.getProperty("properties.load-check"))
				&& "done".equalsIgnoreCase(appProperties.getProperty("queries.load-check")));
	}

}
