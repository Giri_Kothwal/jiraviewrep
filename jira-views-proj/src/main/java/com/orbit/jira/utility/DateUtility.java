package com.orbit.jira.utility;

import java.util.Date;

/**
 * 
 * @author GiriKothwal
 *
 */
public class DateUtility {

	public static int getSubtractDate(Date currentDate, Date date) {
		return (int) ((currentDate.getTime() - date.getTime()) / (1000 * 60 * 60 * 24));
	}

	public static java.sql.Date getSqlDate(Date date) {
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}

	public static Date getUtilDate(java.sql.Date date) {
		java.util.Date utilDate = new java.util.Date(date.getTime());
		return utilDate;
	}
}
