package com.orbit.jira.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author GiriKothwal
 *
 */
public class DBUtility {

	public static Connection getDBConection() throws ClassNotFoundException, SQLException {
		Connection con = null;

		Class.forName(ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewDataSourceDriverClassName));
		con = DriverManager.getConnection(
				ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewDataSourceJDBCUrl),
				ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewDataSourceUsername),
				ConfigurationUtility.getPropertyValue(ConfigurationUtility.jiraViewDataSourcePassword));

		return con;
	}

	public static void closeStatement(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				System.out.println("Could not able to close statement");
				e.printStackTrace();
			}
		}
	}

	public static void closeConnection(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println("Could not able to close connection");
				e.printStackTrace();
			}
		}
	}

	public static void closeDBResources(Connection con, Statement stmt) {
		closeStatement(stmt);
		closeConnection(con);
	}
}
