package com.orbit.jira.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.json.simple.parser.ParseException;
import com.orbit.jira.issues.service.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * 
 * @author GiriKothwal
 *
 */
public class JiraUtility {

	public static final String FLAG_NEW_SRS = "newSRS";
	public static final String FLAG_CLOSED_SRS = "closedSRS";
	public static final String FLAG_OPEN_SRS = "openSRS";
	public static final String FLAG_HISTORY_SRS = "historySRS";

	public static String getEncodedAuthenticationString(String username, String password) {
		String authString = username + ":" + password;
		String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8));
		return authStringEnc;
	}

	public static String constructEncodedUrl(String baseURL, String jqlQuery)
			throws MalformedURLException, UnsupportedEncodingException {
		return baseURL + URLEncoder.encode(jqlQuery, "UTF-8");
	}

	/**
	 * We are calling JIRA rest call and sending response as a String Entity.
	 * 
	 * @param url
	 * @param authStringEnc
	 * @return
	 * @throws URISyntaxException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws IOException
	 * @throws UniformInterfaceException
	 * @throws ClientHandlerException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 */
	public static List<String> callJiraRESTService(String urlString, String authStringEnc)
			throws URISyntaxException, FileNotFoundException, ClientHandlerException, UniformInterfaceException,
			IOException, ParseException, SQLException, ClassNotFoundException {

		String urlStr = null;
		WebResource webResource = null;
		ClientResponse resp = null;
		int count = 0;
		int starAt = 0;
		int maxResults = 100;

		List<String> jsonList = new ArrayList<>();
		Client restClient = Client.create();

		while (true) {

			urlStr = urlString + "&startAt=" + starAt + "&maxResults=" + maxResults;
			URL url = new URL(urlStr);
			webResource = restClient.resource(url.toURI());

			resp = webResource.accept("application/json").header("Authorization", "Basic " + authStringEnc)
					.get(ClientResponse.class);

			if (resp.getStatus() != 200) {
				System.err.println("Unable to get response for [" + url + "]");
			} else {
				String jsonString = resp.getEntity(String.class);
				count = JsonParser.getJiraIssuesCount(jsonString);
				System.out.println(count);
				if (count != 0) {
					starAt = starAt + count;
					jsonList.add(jsonString);
				} else
					break;
			}
		}
		return jsonList;
	}
}
