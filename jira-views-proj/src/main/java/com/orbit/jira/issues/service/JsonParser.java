package com.orbit.jira.issues.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.orbit.jira.pojo.JiraIssue;

/**
 * 
 * @author GiriKothwal
 *
 */
public class JsonParser {

	@SuppressWarnings("unchecked")
	public static int getJiraIssuesCount(String jsonString)
			throws FileNotFoundException, IOException, ParseException, SQLException {

		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(jsonString);
		JSONObject jsonObject = (JSONObject) obj;
		JSONArray jsonArray = (JSONArray) jsonObject.get("issues");
		int count = jsonArray.size();
		return count;
	}

	/**
	 * Parse the provided JSON string and extract Jira Issues as a list
	 * 
	 * @param jsonString
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	public List<JiraIssue> parseJiraIssues(String jsonString)
			throws FileNotFoundException, IOException, ParseException, SQLException {

		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(jsonString);
		List<JiraIssue> jiraIssues = new ArrayList<>();
		JSONObject jsonObject = (JSONObject) obj;
		JSONArray jsonArray = (JSONArray) jsonObject.get("issues");

		jsonArray.forEach(jira -> {
			try {
				JiraIssue jiraIssue = parseJiraIssue((JSONObject) jira);
				jiraIssues.add(jiraIssue);
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
			}
		});

		return jiraIssues;
	}

	/**
	 * Parse Jira Issue out of provided JSON Object
	 * 
	 * @param issue
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private static JiraIssue parseJiraIssue(JSONObject issue) throws SQLException {

		JiraIssue jirapojo = new JiraIssue();

		JSONObject jsonObject = (JSONObject) issue.get("fields");
		JSONObject jsonStatus = (JSONObject) jsonObject.get("status");
		JSONObject jsonAssignee = (JSONObject) jsonObject.get("assignee");

		if (jsonAssignee != null)
			jirapojo.setAssignee((String) jsonAssignee.get(JiraIssue.FLAG_DISPLAY_NAME));

		JSONObject jsonReporter = (JSONObject) jsonObject.get("reporter");
		JSONObject jsonPriority = (JSONObject) jsonObject.get("priority");

		if (jsonObject.get("resolutiondate") != null)
			jirapojo.setResolvedDate(getFormattedDate((String) jsonObject.get("resolutiondate")));
		else
			jirapojo.setResolvedDate(null);

		/*
		 * JSONObject jsonCustomfield = (JSONObject)
		 * jsonObject.get("customfield_12500");// currentStatus
		 * 
		 * if (jsonCustomfield != null) { JSONObject jsonCurrentStatus =
		 * (JSONObject) jsonCustomfield.get("currentStatus"); JSONObject
		 * jsonStatusDate = (JSONObject) jsonCurrentStatus.get("statusDate");
		 * jirapojo.setResolvedDate(getFormattedDate((String)
		 * jsonStatusDate.get("iso8601"))); }
		 */

		jirapojo.setSrKey((String) issue.get("key"));
		jirapojo.setSummary((String) jsonObject.get("summary"));
		jirapojo.setStatus((String) jsonStatus.get(JiraIssue.FLAG_NAME));
		jirapojo.setCreatedDate(getFormattedDate((String) jsonObject.get("created")));

		if (jsonObject.get("updated") != null)
			jirapojo.setUpdatedDate(getFormattedDate((String) jsonObject.get("updated")));
		else
			jirapojo.setUpdatedDate(getFormattedDate((String) jsonObject.get("created")));

		jirapojo.setReporter((String) jsonReporter.get(JiraIssue.FLAG_DISPLAY_NAME));
		jirapojo.setPriority((String) jsonPriority.get(JiraIssue.FLAG_NAME));

		JSONArray jsonCustomer = (JSONArray) jsonObject.get("customfield_12200");// customer
		jsonCustomer.forEach(jira -> parseCustomer((JSONObject) jira, jirapojo));

		if (jsonCustomer.isEmpty()) {
			jirapojo.setCustomer("Unidentified");
		}

		JSONArray jsonComponents = (JSONArray) jsonObject.get("components");
		jsonComponents.forEach(jira -> parseComponents((JSONObject) jira, jirapojo));

		JSONArray labels = (JSONArray) jsonObject.get("labels");

		if (labels.size() == 0) {
			jirapojo.setRegression("N");
			jirapojo.setLabels(null);
		} else {
			regression(labels, jirapojo);
		}

		return jirapojo;
	}

	/**
	 * When more than one component exist, they are composed as comma separated
	 * strings
	 * 
	 * @param jsonComponent
	 * @param jiraPOJO
	 */
	@SuppressWarnings("unused")
	private static void parseComponents(JSONObject jsonComponent, JiraIssue jiraPOJO) // components
	{
		String componentName = null;

		if (componentName != null) {
			componentName = componentName + ", " + (String) jsonComponent.get(JiraIssue.FLAG_NAME);
			jiraPOJO.setComponents(componentName);
		} else {
			componentName = (String) jsonComponent.get(JiraIssue.FLAG_NAME);
			jiraPOJO.setComponents(componentName);
		}
	}

	/**
	 * Parse customer name
	 * 
	 * @param jsonCustomer
	 * @param jiraPOJO
	 */
	private static void parseCustomer(JSONObject jsonCustomer, JiraIssue jirapojo) {
		jirapojo.setCustomer((String) jsonCustomer.get(JiraIssue.FLAG_NAME));
	}

	private static void regression(JSONArray labels, JiraIssue jirapojo) {
		String regressionArray = null;

		for (Object object : labels) {
			String regression = (String) object;

			if (regression != null) {
				if (regressionArray == null)
					regressionArray = "\"" + regression + "\"" + ",";
				else
					regressionArray = regressionArray + "\"" + regression + "\"" + ",";

			}

			if (regression.contains(JiraIssue.FLAG_REGRESSION))
				jirapojo.setRegression("Y");
			else
				jirapojo.setRegression("N");
		}

		if (regressionArray.endsWith(",")) {
			regressionArray = regressionArray.substring(0, regressionArray.length() - 1);
		}

		jirapojo.setLabels(regressionArray);
	}

	private static Date getFormattedDate(String dateTime) {
		Date date = null;
		try {
			String str = dateTime.replaceAll("[a-zA-Z]", " ");
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
}
