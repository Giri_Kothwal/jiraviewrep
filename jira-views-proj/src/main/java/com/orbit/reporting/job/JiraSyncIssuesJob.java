package com.orbit.reporting.job;

import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orbit.jira.rest.JiraClient;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class JiraSyncIssuesJob extends OrbitQuartzJob {
	private final Logger logger;
	public static final String AUTHORITIES = "AUTHORITIES";

	public JiraSyncIssuesJob() {
		this.logger = LoggerFactory.getLogger((Class) JiraSyncIssuesJob.class);
	}

	public void execute(final JobExecutionContext context) throws JobExecutionException {
		this.logger.info("JiraSyncIssues Job is started. Job key is {}", (Object) context.getJobDetail().getKey());
		final JobDataMap dataMap = context.getMergedJobDataMap();
		final String error = null;
		SchedulerContext schedulerContext = null;
		try {
			schedulerContext = context.getScheduler().getContext();
			this.logger.info("before calling jira pkg {}", (Object) context.getJobDetail().getKey());
			final JiraClient jiraClient = new JiraClient();
			final Date currentDate = new Date();
			try {
				jiraClient.populateSRInformation();
			} catch (Exception e) {
				this.logger.info("in Exception {}", (Object) e.getMessage());
				e.printStackTrace();
			}
			this.logger.info("After calling jira pkg {}", (Object) context.getJobDetail().getKey());
		} catch (SchedulerException e2) {
			this.logger.error("Error when getting sechedulter context in SyncUsersJob", (Throwable) e2);
		}
	}
}