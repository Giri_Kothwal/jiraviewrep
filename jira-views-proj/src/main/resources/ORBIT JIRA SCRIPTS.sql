/* Formatted on 1/30/2020 8:24:36 PM (QP5 v5.139.911.3011) */
/*BEGIN

   DELETE FROM OS_Jira_Open_SRs;                                          --50

   DELETE FROM OS_Jira_Closed_SRs;                                         --9

   DELETE FROM OS_Jira_New_SRs;                                            --7

   DELETE FROM OS_Jira_SR_history;

   DELETE FROM OS_Jira_SR_history_bkp;
END;

SELECT * FROM OS_Jira_Open_SRs;

SELECT * FROM OS_Jira_Closed_SRs;

SELECT * FROM OS_Jira_New_SRs;

SELECT * FROM OS_Jira_SR_history

SELECT * FROM OS_Jira_SR_history_bkp;*/


---------------------------------------------------------SEQUENCE -------------------------------------------

CREATE SEQUENCE OS_Jira_New_SRs_s
   START WITH 10000
   INCREMENT BY 1
   NOCACHE
   NOCYCLE;
/
CREATE SEQUENCE OS_week_dimensions_s
   START WITH 10000
   INCREMENT BY 1
   NOCACHE
   NOCYCLE;
/

CREATE SEQUENCE OS_Jira_Closed_SRs_s
   START WITH 10000
   INCREMENT BY 1
   NOCACHE
   NOCYCLE;
/
CREATE SEQUENCE OS_Jira_Open_SRs_s
   START WITH 10000
   INCREMENT BY 1
   NOCACHE
   NOCYCLE;
 /  
   
 CREATE SEQUENCE OS_JIRA_SR_HISTORY_s
   START WITH 10000
   INCREMENT BY 1
   NOCACHE
   NOCYCLE;  
   
  /   
   

--------------------------------------------------------- Tables --------------------------------------------

CREATE TABLE OS_week_dimensions
(
   week_dimensions_id   INT PRIMARY KEY,
   week_start_date      DATE,
   week_end_date        DATE,
   WEEK_DESCRIPTION     VARCHAR2 (40 BYTE)
);
/



CREATE TABLE OS_JIRA_NEW_SRS
(
   NEW_JIRA_SR_ID   INT PRIMARY KEY,
   SR_KEY           VARCHAR2 (50 BYTE) NOT NULL,
   ASSIGNEE         VARCHAR2 (100 BYTE),
   REPORTER         VARCHAR2 (100 BYTE) NOT NULL,
   PRIORITY         VARCHAR2 (50 BYTE) NOT NULL,
   STATUS           VARCHAR2 (50 BYTE) NOT NULL,
   CREATED          DATE NOT NULL,
   UPDATED          DATE NOT NULL,
   RESOLVED         DATE,
   COMPONENTS       VARCHAR2 (200 BYTE),
   CUSTOMER         VARCHAR2 (200 BYTE) NOT NULL,
   SUMMARY          VARCHAR2 (4000 BYTE) NOT NULL,
   WEEK_ID          INTEGER NOT NULL,
   CONSTRAINT week_id FOREIGN KEY
      (week_id)
       REFERENCES OS_week_dimensions (week_dimensions_id),
   IS_REGRESSION    VARCHAR2 (1 BYTE),
   LABELS VARCHAR2(1000)
)
/

CREATE TABLE OS_JIRA_CLOSED_SRS
(
   CLOSED_JIRA_SR_ID   INT PRIMARY KEY,
   SR_KEY              VARCHAR2 (50 BYTE) NOT NULL,
   ASSIGNEE            VARCHAR2 (100 BYTE),
   REPORTER            VARCHAR2 (100 BYTE) NOT NULL,
   PRIORITY            VARCHAR2 (50 BYTE) NOT NULL,
   STATUS              VARCHAR2 (50 BYTE) NOT NULL,
   CREATED             DATE NOT NULL,
   UPDATED             DATE NOT NULL,
   RESOLVED            DATE,
   COMPONENTS          VARCHAR2 (200 BYTE),
   CUSTOMER            VARCHAR2 (200 BYTE) NOT NULL,
   SUMMARY             VARCHAR2 (4000 BYTE) NOT NULL,
   WEEK_ID             INTEGER NOT NULL,
   CONSTRAINT week_id_fk FOREIGN KEY
      (week_id)
       REFERENCES OS_week_dimensions (week_dimensions_id),
   IS_REGRESSION       VARCHAR2 (1 BYTE),
   LABELS VARCHAR2(1000)
)
/

CREATE TABLE OS_JIRA_OPEN_SRS
(
   OPEN_JIRA_SR_ID             INT PRIMARY KEY,
   SR_KEY                      VARCHAR2 (50 BYTE) NOT NULL,
   ASSIGNEE                    VARCHAR2 (100 BYTE),
   REPORTER                    VARCHAR2 (100 BYTE) NOT NULL,
   PRIORITY                    VARCHAR2 (50 BYTE) NOT NULL,
   STATUS                      VARCHAR2 (50 BYTE) NOT NULL,
   CREATED                     DATE NOT NULL,
   UPDATED                     DATE NOT NULL,
   RESOLVED                    DATE,
   COMPONENTS                  VARCHAR2 (200 BYTE),
   CUSTOMER                    VARCHAR2 (200 BYTE) NOT NULL,
   SUMMARY                     VARCHAR2 (4000 BYTE) NOT NULL,
   AGE                         INTEGER NOT NULL,
   AGING_BUCKET                VARCHAR2 (200 BYTE) NOT NULL,
   LAST_UPDATED_AGING          INTEGER NOT NULL,
   LAST_UPDATED_AGING_BUCKET   VARCHAR2 (200 BYTE) NOT NULL,
   WEEK_ID                     INTEGER NOT NULL,
   CONSTRAINT open_sr_week_id FOREIGN KEY
      (week_id)
       REFERENCES OS_week_dimensions (week_dimensions_id),
   IS_REGRESSION               VARCHAR2 (1 BYTE),
   LABELS VARCHAR2(1000)
)
/

CREATE TABLE OS_JIRA_SR_HISTORY
(
   JIRA_SR_ID      INT PRIMARY KEY,
   SR_KEY          VARCHAR2 (50 BYTE) NOT NULL,
   ASSIGNEE        VARCHAR2 (100 BYTE),
   REPORTER        VARCHAR2 (100 BYTE) NOT NULL,
   PRIORITY        VARCHAR2 (50 BYTE) NOT NULL,
   STATUS          VARCHAR2 (50 BYTE) NOT NULL,
   CREATED         DATE NOT NULL,
   UPDATED         DATE NOT NULL,
   RESOLVED        DATE,
   COMPONENTS      VARCHAR2 (200 BYTE),
   CUSTOMER        VARCHAR2 (200 BYTE),
   SUMMARY         VARCHAR2 (4000 BYTE) NOT NULL,
   WEEK_ID         INTEGER NOT NULL,
   CONSTRAINT sr_week_id FOREIGN KEY
      (week_id)
       REFERENCES OS_week_dimensions (week_dimensions_id),
   IS_REGRESSION   VARCHAR2 (1 BYTE),
   LABELS VARCHAR2(1000)
)
/

CREATE TABLE OS_JIRA_SR_HISTORY_BKP
(
   JIRA_SR_ID      INTEGER ,
   SR_KEY          VARCHAR2 (50 BYTE) NOT NULL,
   ASSIGNEE        VARCHAR2 (100 BYTE),
   REPORTER        VARCHAR2 (100 BYTE) NOT NULL,
   PRIORITY        VARCHAR2 (50 BYTE) NOT NULL,
   STATUS          VARCHAR2 (50 BYTE) NOT NULL,
   CREATED         DATE NOT NULL,
   UPDATED         DATE NOT NULL,
   RESOLVED        DATE,
   COMPONENTS      VARCHAR2 (200 BYTE),
   CUSTOMER        VARCHAR2 (200 BYTE),
   SUMMARY         VARCHAR2 (4000 BYTE) NOT NULL,
   WEEK_ID         INTEGER NOT NULL,
   IS_REGRESSION   VARCHAR2 (1 BYTE),
   LABELS VARCHAR2(1000)
)
/

CREATE OR REPLACE PROCEDURE get_history_bkp_jira_week_id (
   p_week_dimensions_id IN NUMBER)
IS
   l_open_jira_week_id    NUMBER := 0;
   --testing
   l_week_dimensions_id   NUMBER := p_week_dimensions_id;
BEGIN
   SELECT week_id
     INTO l_open_jira_week_id
     FROM OS_Jira_SR_history
    WHERE week_id = l_week_dimensions_id AND ROWNUM < 2;

   BEGIN
      IF (l_open_jira_week_id > 0)
      THEN
         DBMS_OUTPUT.put_line ('inside if');

         EXECUTE IMMEDIATE 'TRUNCATE TABLE OS_Jira_SR_history_bkp';

         INSERT INTO OS_Jira_SR_history_bkp
                 SELECT * FROM OS_Jira_SR_history;

         EXECUTE IMMEDIATE 'TRUNCATE TABLE OS_Jira_SR_history';

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.
         put_line (
            'Exception while inserting into OS_Jira_SR_history_bkp table');
   END;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('no data found OS_Jira_SR_history  table');
END get_history_bkp_jira_week_id;
/


---------------------------------------------------function to check week_dimensions_id-------------------------------------------

CREATE OR REPLACE FUNCTION get_week_dimensions_id (p_week_date IN DATE)
   RETURN NUMBER
IS
   l_week_dimensions_id   NUMBER := 0;
BEGIN
   SELECT week_dimensions_id
     INTO l_week_dimensions_id
     FROM OS_week_dimensions
    WHERE TRUNC (p_week_date) BETWEEN week_start_date AND week_end_date AND ROWNUM < 2;

   RETURN (l_week_dimensions_id);
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN (l_week_dimensions_id);
END get_week_dimensions_id;
/



-----------------------------------------------------function to get new_jira_sr_id----------

CREATE OR REPLACE FUNCTION get_new_jira_week_id (
   p_week_dimensions_id IN NUMBER)
   RETURN NUMBER
IS
   l_new_jira_week_id   NUMBER := 0;
BEGIN
   SELECT week_id
     INTO l_new_jira_week_id
     FROM OS_Jira_New_SRs
    WHERE week_id = p_week_dimensions_id AND ROWNUM < 2;

   RETURN (l_new_jira_week_id);
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN (l_new_jira_week_id);
END get_new_jira_week_id;
/



------------------------------------------function to get closed_jira_sr_id--------------------------------

CREATE OR REPLACE FUNCTION get_closed_jira_week_id (
   p_week_dimensions_id IN NUMBER)
   RETURN NUMBER
IS
   l_closed_jira_week_id   NUMBER := 0;
BEGIN
   SELECT week_id
     INTO l_closed_jira_week_id
     FROM OS_Jira_Closed_SRs
    WHERE week_id = p_week_dimensions_id AND ROWNUM < 2;

   RETURN (l_closed_jira_week_id);
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN (l_closed_jira_week_id);
--  raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END get_closed_jira_week_id;
/


----------------------------------------------function------------------

CREATE OR REPLACE FUNCTION get_open_jira_week_id (
   p_week_dimensions_id IN NUMBER)
   RETURN NUMBER
IS
   l_open_jira_week_id   NUMBER := 0;
BEGIN
   SELECT week_id
     INTO l_open_jira_week_id
     FROM OS_Jira_Open_SRs
    WHERE week_id = p_week_dimensions_id AND ROWNUM < 2;

   RETURN (l_open_jira_week_id);
EXCEPTION
   WHEN OTHERS
   THEN
       RETURN (l_open_jira_week_id);
END get_open_jira_week_id;
/

--------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_history_jira_week_id (
   p_week_dimensions_id IN NUMBER)
   RETURN NUMBER
IS
   l_history_jira_week_id   NUMBER := 0;
BEGIN
   SELECT week_id
     INTO l_history_jira_week_id
     FROM OS_Jira_SR_history
    WHERE week_id = p_week_dimensions_id AND ROWNUM < 2;

   RETURN (l_history_jira_week_id);
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN (l_history_jira_week_id);
--  raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END get_history_jira_week_id;
/

CREATE OR REPLACE FUNCTION get_sysdate_in_IST
RETURN DATE AS
BEGIN
   RETURN CAST( SYSTIMESTAMP AT TIME ZONE '+05:30' AS DATE);
END get_sysdate_in_IST;
/
----------------------------------------------------------------------------------------------------------

DECLARE
   V_Counter      NUMBER;
   V_Start_Date   DATE;
   V_End_Date     DATE;
   V_Count        NUMBER := 0;
   v_date_diff    NUMBER := 0;
BEGIN
   V_Counter := 1;
   V_Start_Date := TO_DATE ('1 Jan 2018', 'DD MON YYYY');

   BEGIN
      SELECT COUNT (*)
        INTO V_Count
        FROM Os_Week_Dimensions
       WHERE TRUNC (V_Start_Date) BETWEEN Week_Start_Date AND Week_End_Date;
   EXCEPTION
      WHEN OTHERS
      THEN
         Raise_Application_Error (
            -20001,
               'An error was encountered - '
            || SQLCODE
            || ' -ERROR- '
            || SQLERRM);
   END;

   IF V_Count = 0
   THEN
      v_date_diff := ROUND (SYSDATE - TO_DATE ('1 Jan 2018', 'DD MON YYYY'));

      FOR V_Counter IN 1 .. v_date_diff
      LOOP
         DBMS_OUTPUT.Put_Line ('actual date = ' || V_Start_Date);
         V_End_Date := V_Start_Date + 6;
         DBMS_OUTPUT.Put_Line ('week date = ' || V_End_Date);

         INSERT INTO Os_Week_Dimensions (Week_Dimensions_Id,
                                         Week_Start_Date,
                                         Week_End_Date,
                                         week_Description)
              VALUES (
                 Os_Week_Dimensions_S.NEXTVAL,
                 TRUNC (V_Start_Date),
                 TRUNC (V_End_Date),
                    TO_CHAR (v_start_date, 'DD/MON/YYYY')
                 || ' - '
                 || TO_CHAR (v_end_date, 'DD/MON/YYYY'));

         V_Start_Date := V_End_Date + 1;
      END LOOP;

      COMMIT;
   ELSE
      DBMS_OUTPUT.
      Put_Line ('already data is existed with this date' || V_Start_Date);
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      Raise_Application_Error (
         -20001,
         'An error was encountered - ' || SQLCODE || ' -ERROR- ' || SQLERRM);
END;
/
------------------------------------------------------------------------------------------------------------------------
