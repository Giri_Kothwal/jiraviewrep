package com.orbit.jira.rest.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.parser.ParseException;

import com.orbit.jira.rest.JiraClient;

/**
 * 
 * @author GiriKothwal
 *
 */
public class JiraClientTest {

	public static void main(String a[]) {
		JiraClient jiraClient = new JiraClient();
		try {
			Date currentDate = new Date();

			Calendar calendar = Calendar.getInstance();
			System.out.println("Current Date = " + calendar.getTime());
			// Decrementing days by 2
			calendar.add(Calendar.DATE, +8);
			System.out.println("Updated Date = " + calendar.getTime());

			Date d = calendar.getTime();
			/*
			 * Calendar c = Calendar.getInstance(); c.setTime(currentDate);
			 * c.add(Calendar.MONTH, 1); currentDate = c.getTime();
			 */
			jiraClient.populateSRInformation();
		} catch (ClassNotFoundException | URISyntaxException | IOException | SQLException | ParseException e) {
			e.printStackTrace();
		}
	}
}
